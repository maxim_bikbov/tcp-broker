cmake_minimum_required(VERSION 3.5)

project(tcp_broker LANGUAGES C)

include_directories(include)
add_subdirectory(tcp_broker_client)
add_subdirectory(tcp_broker_server)
