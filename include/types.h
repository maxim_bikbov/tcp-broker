#ifndef TYPES_H
#define TYPES_H

#define E_CODE -1
#define E_OK_CODE 0
#define IPV4_STR_ADDR_LEN 16
#define MAX_MSG_LEN 80

#endif // TYPES_H
