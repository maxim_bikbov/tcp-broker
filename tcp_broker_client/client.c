#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>
#include "types.h"
#define FILE_LOG_NAME 30

typedef struct sockaddr sockaddrs_s;

int _print_log_to_file(int sockfd, char *msg)
{
    char log_file_name[FILENAME_MAX];
    memset(log_file_name, 0, FILENAME_MAX);
    sprintf(log_file_name,"client_%d.log", sockfd);
    FILE *f = fopen(log_file_name, "a+");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        return E_CODE;
    }
    fprintf(f, "Receive message: \"%s\"\n", msg);
    fclose(f);
}

void *wait_input(void *arg)
{
    char buff[MAX_MSG_LEN];
    int sockfd = *(int *)arg;
    int n;
    int ret = 0;
    while(1)
    {
        memset(buff, 0, sizeof(buff));
        printf("Enter the message (max length 80): ");
        n = 0;
        int symbol = getchar();
        while (symbol != '\n')
        {
            buff[n++] = (char)symbol;
            symbol = getchar();
            if(n >= 80)
            {
                printf("\nMessage length must be less than "
                       "80 symbols.\n");
                // clear symbols which over 80
                while (symbol != '\n')
                    symbol = getchar();
                memset(buff, 0, sizeof(buff));
                n = E_CODE;
            }
        }
        if(n != E_CODE)
        {
            ret = write(sockfd, buff, sizeof(buff));
            if(ret == E_CODE)
            {
                printf("Client error send message to server(%s)\n",
                       strerror(errno));
                break;
            }
        }
    }
    return NULL;
}

void *wait_msg(void *arg)
{
    int sockfd = *(int *)arg;
    char buff[MAX_MSG_LEN];
    int ret = E_OK_CODE;
    while(1)
    {
        memset(buff, 0, MAX_MSG_LEN);
        ret = read(sockfd, buff, sizeof(buff));
        if(ret == E_CODE)
        {
            printf("Error get message from client (%d)(%s)\n",
                   sockfd, strerror(errno));
            break;
        }
        if(ret == 0)
        {
            printf("\nServer disconnected\n");
            break;
        }
        _print_log_to_file(sockfd, buff);
    }
    return NULL;
}

int init_client(struct sockaddr_in *server_addr)
{
    int server_sockfd;

    server_sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_sockfd == E_CODE)
    {
        printf("Socket creation failed(%s)\n", strerror(errno));
        return E_CODE;
    }
    printf("Socket successfully created\n");
    if (connect(server_sockfd,
                (sockaddrs_s *)server_addr,
                sizeof(struct sockaddr_in)) == E_CODE)
    {
        printf("Connection with the server failed(%s)\n", strerror(errno));
        return E_CODE;
    }
    printf("Connected to the server. sockfd is %d\n", server_sockfd);

    return server_sockfd;
}
