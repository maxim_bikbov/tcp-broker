#ifndef CLIENT_H
#define CLIENT_H

#include <netinet/in.h>

/****************************************************************************
 * Name:          init_client                                               *
 * Description:   Init client                                               *
 * Parameters:    server_addr(in) :information about server IP address      *
 *                and port                                                  *
 * Return Values: client socker file descriptor or E_CODE                   *
 ***************************************************************************/
int init_client(struct sockaddr_in *server_addr);

/****************************************************************************
 * Name:          wait_msg                                                  *
 * Description:   Wait messages from client and processing its              *
 * Parameters:    arg(in) :client socker file descriptor                    *
 * Return Values:                                                           *
 ***************************************************************************/
void *wait_msg(void *arg);

/****************************************************************************
 * Name:          wait_input                                                *
 * Description:   Wait user input and send to server                        *
 * Parameters:    arg(in) :client socker file descriptor                    *
 * Return Values:                                                           *
 ***************************************************************************/
void *wait_input(void *arg);

#endif // CLIENT_H
