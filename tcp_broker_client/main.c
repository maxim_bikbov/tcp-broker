#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <errno.h>
#include <limits.h>
#include <ctype.h>
#include<pthread.h>
#include "client.h"
#include "types.h"

int main(int argc, char **argv)
{
    char *port_str = NULL;
    char *ip_str = NULL;
    int i;
    long port;
    int ret, server_sockfd;
    struct sockaddr_in server_addr;
    unsigned int ip_addr = 0;
    pthread_t thread_read;
    pthread_t thread_write;
    memset(&server_addr, 0, sizeof (server_addr));
    /* 1. app name
     * 2. port or ip flag
     * 3. port or ip data
     * 4. the same 2
     * 5. the same 3
     */
    if(argc == 5)
    {
        for(i = 1; i < argc; ++i)
        {
            if(strcmp(argv[i], "-p") == 0)
            {
                port_str = argv[i+1];
                ++i;
            }
            if(strcmp(argv[i], "-ip") == 0)
            {
                ip_str = argv[i+1];
                ++i;
            }
        }
    }
    else
    {
        printf("Wrong arguments.\n"
               "\tUse: tcp_broker_client -p port -ip ip_address\n");
        return E_CODE;
    }

    if(ip_str == NULL || port_str == NULL)
    {
        printf("Wrong arguments.\n"
               "\tUse: tcp_broker_client -p port -ip ip_address\n");
        return E_CODE;
    }

    if(inet_pton(AF_INET, ip_str, &(ip_addr)) != 1)
    {
        printf("Wrong IP address\n");
        return E_CODE;
    }
    server_addr.sin_addr.s_addr = ip_addr;
    for(i = 0; i < strlen(port_str); ++i)
    {
        if(!isdigit(port_str[i]))
        {
            printf("Wrong port(%s)\n", strerror(errno));
            return E_CODE;
        }
    }
    port = strtol(port_str, NULL,10);
    if(port == 0 || port > USHRT_MAX)
    {
        printf("Wrong port(%s)\n", strerror(errno));
        return E_CODE;
    }
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons((unsigned short)port);

    server_sockfd = init_client(&server_addr);
    if(server_sockfd == E_CODE)
    {
        printf("Failed init client\n");
        return E_CODE;
    }

    // function for wait messages from another clients
    ret = pthread_create(&(thread_read), NULL,
                         wait_msg, (void *)&server_sockfd);
    if(ret != E_OK_CODE)
    {
        printf("Failed pthread_create with error(%d): %s\n",
               ret, strerror(ret));
        return ret;
    }
    // function for wait user input
    ret = pthread_create(&(thread_write), NULL,
                         wait_input, (void *)&server_sockfd);
    if(ret != E_OK_CODE)
    {
        printf("Failed pthread_create with error(%d): %s\n",
               ret, strerror(ret));
        return ret;
    }

    ret = pthread_join(thread_read, NULL);
    if(ret != E_OK_CODE)
    {
        printf("Failed pthread_detach with error(%d): %s\n",
               ret, strerror(ret));
        return ret;
    }
    ret = pthread_cancel(thread_write);
    if(ret != E_OK_CODE)
    {
        printf("Failed pthread_detach with error(%d): %s\n",
               ret, strerror(ret));
        return ret;
    }

    ret = close(server_sockfd);
    if(ret != E_OK_CODE)
    {
        printf("Failed close socket(%s)\n", strerror(ret));
        return E_CODE;
    }

    printf("Exited from client\n");
    return 0;
}
