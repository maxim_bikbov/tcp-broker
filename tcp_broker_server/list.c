#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "list.h"

list_node_s* new_list_node(int client_sockfd, struct sockaddr_in client_addr)
{
    list_node_s *node = (list_node_s *)malloc(sizeof (list_node_s));
    if(node == NULL)
        return NULL;
    node->client_sockfd = client_sockfd;
    memcpy(&node->client_addr, &client_addr, sizeof (client_addr));
    return  node;
}

int _delete_list_node(list_s *list, list_node_s *node)
{
    if(list == NULL || node == NULL)
        return E_CODE;
    if(node->next == NULL)
    {
        if(node->prev != NULL)
        {
            list->tail = node->prev;
            node->prev->next = NULL;
        }
        else
        {
            list->head = list->tail = NULL;
        }
    }
    else if(node->prev == NULL)
    {
        if(node->next != NULL)
        {
            list->head = node->next;
            node->next->prev = NULL;
        }
    }
    else
    {
        node->prev->next = node->next;
        node->next->prev = node->prev;
    }
    --list->size;
    free(node);
    return E_OK_CODE;
}

int _cmp_list_node(list_node_s * node_1, list_node_s *node_2)
{
    if(node_1 == NULL || node_2 == NULL)
        return E_CODE;
    if(node_1->client_sockfd == node_2->client_sockfd)
        return 0;
    else
        return -1;
}

int add_to_list(list_s *list, list_node_s *add_node)
{
    if(list == NULL || add_node == NULL)
        return E_CODE;
    //First node
    if(list->tail == NULL && list->head == NULL)
    {
        add_node->prev = add_node->next = NULL;
        list->tail = list->head = add_node;
        ++list->size;
        return E_OK_CODE;
    }

    list->tail->next = add_node;
    add_node->prev = list->tail;
    add_node->next = NULL;
    list->tail = add_node;
    ++list->size;
    return E_OK_CODE;
}

int remove_from_list(list_s *list, list_node_s *node)
{
    if(list == NULL || node == NULL)
        return E_CODE;
    list_node_s *iter_node;
    for(iter_node = list->head; iter_node != NULL; iter_node = iter_node->next)
    {
        if(_cmp_list_node(iter_node, node) == 0)
            return _delete_list_node(list, iter_node);
    }
    return E_CODE;
}

int remove_from_list_by_sockfd(list_s *list, int sockfd)
{
    if(list == NULL || sockfd == 0)
        return E_CODE;
    list_node_s *iter_node;
    for(iter_node = list->head; iter_node != NULL; iter_node = iter_node->next)
    {
        if(iter_node->client_sockfd == sockfd)
            return _delete_list_node(list, iter_node);
    }
    return E_CODE;
}

list_s *init_list()
{
    list_s *list = (list_s *)malloc(sizeof (list_s));
    return list;
}

void clear_and_remove_list(list_s *list)
{
    if(list != NULL)
    {
        list_node_s *node;
        for(node = list->head; node != NULL; node = node->next)
        {
            free(node);
            --list->size;
        }
        free(list);
    }
}
