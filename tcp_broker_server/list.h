#ifndef LIST_H
#define LIST_H

#include <netinet/in.h>

struct list_node {
    int client_sockfd;
    struct sockaddr_in client_addr;
    struct list_node* next;
    struct list_node* prev;
} typedef list_node_s;

struct list {
    struct list_node* head;
    struct list_node* tail;
    size_t size;
} typedef list_s;

/****************************************************************************
 * Name:          new_list_node                                             *
 * Description:   Allocate memory and fill data for new list node           *
 * Parameters:    client_sockfd(in) :client socket file descriptor          *
 *                client_addr(in) :struct wich contain IP and port of client*
 * Return Values: Pointer to allocated memory or NULL                       *
 ***************************************************************************/
list_node_s* new_list_node(int client_sockfd, struct sockaddr_in client_addr);

/****************************************************************************
 * Name:          add_to_list                                               *
 * Description:   Add node to list                                          *
 * Parameters:    list(in) :list of clients                                 *
 *                add_node(in) :new node which will add to list             *
 * Return Values: E_OK_CODE or E_CODE                                       *
 ***************************************************************************/
int add_to_list(list_s *list, list_node_s *add_node);

/****************************************************************************
 * Name:          remove_from_list                                          *
 * Description:   Remove node from list                                     *
 * Parameters:    list(in) :list of clients                                 *
 *                node(in) :node which will remove from list                *
 * Return Values: E_OK_CODE or E_CODE                                       *
 ***************************************************************************/
int remove_from_list(list_s *list, list_node_s *node);

/****************************************************************************
 * Name:          remove_from_list_by_sockfd                                *
 * Description:   Remove node from list by client sockfd                    *
 * Parameters:    list(in) :list of clients                                 *
 *                sockfd(in) :client socket file descriptor                 *
 * Return Values: E_OK_CODE or E_CODE                                       *
 ***************************************************************************/
int remove_from_list_by_sockfd(list_s *list, int sockfd);

/****************************************************************************
 * Name:          init_list                                                 *
 * Description:   Allocate memory for list                                  *
 * Parameters:                                                              *
 * Return Values: Pointer to allocated memory or NULL                       *
 ***************************************************************************/
list_s *init_list();

/****************************************************************************
 * Name:          clear_and_remove_list                                     *
 * Description:   Clear and remove list                                     *
 * Parameters:    list(in) :list of clients                                 *
 * Return Values:                                                           *
 ***************************************************************************/
void clear_and_remove_list(list_s *list);

#endif // LIST_H
