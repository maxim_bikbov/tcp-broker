#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <ctype.h>
#include <errno.h>
#include<pthread.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include "list.h"
#include "types.h"
#include "server.h"

#define MAX_WAIT_CONN 10

list_s *list_of_client;
pthread_mutex_t mutex;

int main(int argc, char **argv)
{
    char *port_str = NULL;
    unsigned long port;
    int i, server_sockfd, ret = E_OK_CODE;
    /* 1. app name
     * 2. port flag
     * 3. port data
    */
    if(argc == 3)
    {
        for(i = 1; i < argc; ++i)
        {
            if(strcmp(argv[i], "-p") == 0)
            {
                port_str = argv[i+1];
                ++i;
            }
        }
    }
    else
    {
        printf("Wrong arguments.\n"
               "\tUse: tcp_broker_server -p port \n");
        return E_CODE;
    }

    if(port_str == NULL)
    {
        printf("Wrong arguments.\n"
               "\tUse: tcp_broker_server -p port \n");
        return E_CODE;
    }

    for(i = 0; i < strlen(port_str); ++i)
    {
        if(!isdigit(port_str[i]))
        {
            printf("Wrong port(%s)\n", strerror(errno));
            return E_CODE;
        }
    }
    port = strtol(port_str, NULL,10);
    if(port == 0 || port > USHRT_MAX)
    {
        printf("Wrong port(%s)\n", strerror(errno));
        return E_CODE;
    }

    list_of_client =  init_list();
    if(list_of_client == NULL)
    {
        printf("Failed init list of clients\n");
        return 0;
    }
    server_sockfd = init_server(MAX_WAIT_CONN, port);
    if(server_sockfd == E_CODE)
    {
        printf("Failed init server\n");
        return E_CODE;
    }

    ret = pthread_mutex_init(&mutex, NULL);
    if(ret != E_OK_CODE)
    {
        printf("Failed pthread_mutex_init with error(%d): %s\n",
               ret, strerror(ret));
        return ret;
    }

    ret = wait_clients(server_sockfd);
    if(ret == E_CODE)
    {
        printf("Failed wait clients\n");
    }

    clear_and_remove_list(list_of_client);

    ret = pthread_mutex_destroy(&mutex);
    if(ret != E_OK_CODE)
    {
        printf("Failed pthread_mutex_destroy with error(%d): %s\n",
               ret, strerror(ret));
        return E_CODE;
    }
    ret = close(server_sockfd);
    if(ret != E_OK_CODE)
    {
        printf("Failed close socket(%s)\n", strerror(ret));
        return E_CODE;
    }

    return 0;
}
