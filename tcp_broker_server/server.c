#include <stdio.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <errno.h>
#include "types.h"
#include "list.h"
#include "server.h"

typedef struct sockaddr sockaddrs_s;

extern list_s *list_of_client;
extern pthread_mutex_t mutex;

int _print_all_clients(list_s *list)
{
    if(list == NULL)
        return E_CODE;
    list_node_s *node;
    char ip_str[IPV4_STR_ADDR_LEN];
    for(node= list->head; node != NULL; node = node->next)
    {
        if(inet_ntop(AF_INET, &node->client_addr.sin_addr,
                     ip_str, IPV4_STR_ADDR_LEN) == NULL)
        {
            printf("Failed convert IP address to string(%s)\n",
                   strerror(errno));
            return E_CODE;
        }
        printf("Client IP address: %s\n"
               "Client port: %d\n"
               "Client sockfd: %d\n",
               ip_str,
               node->client_addr.sin_port,
               node->client_sockfd);
    }
    return E_OK_CODE;
}

int send_msg_to_client(int sockfd, char* msg, unsigned long msg_len)
{
    int ret = E_OK_CODE;
    ret = write(sockfd, msg, msg_len);
    if(ret == E_CODE)
    {
        printf("Server error send msg to client(%s)\n", strerror(errno));
        return ret;
    }
    return ret;
}

int send_msg_to_all_clients(list_s *list,
                            int sender_sockfd,
                            char* msg, int msg_len)
{
    if(list == NULL)
        return E_CODE;
    list_node_s *node;
    int ret = 0;
    for(node = list->head; node != NULL; node = node->next)
    {
        if(node->client_sockfd != sender_sockfd)
        {
            ret = send_msg_to_client(node->client_sockfd, msg, msg_len);
            if(ret == E_CODE)
            {
                printf("Failed send message \"%s\" to client with sockfd %d\n",
                       msg, node->client_sockfd);
            }
            else
            {
                printf("Succesfully sended message \"%s\" to client"
                       " with sockfd %d\n", msg, node->client_sockfd);
            }
        }
    }
    return E_OK_CODE;
}

int add_client_to_list(int client_sockfd, struct sockaddr_in client_addr)
{
    int ret = E_OK_CODE;
    list_node_s *node = new_list_node(client_sockfd, client_addr);
    if(node == NULL)
    {
        printf("Failed allocate client node\n");
        return E_CODE;
    }
    printf("Client list node allocated\n");
    pthread_mutex_lock(&mutex);
        ret = add_to_list(list_of_client, node);
    pthread_mutex_unlock(&mutex);
    if(ret == E_CODE)
    {
        printf("Failed add client to list\n");
        return ret;
    }
    printf("Client added to list of clients\n");
    return ret;
}

int remove_client_from_list(int client_sockfd)
{
    int ret = E_OK_CODE;
    pthread_mutex_lock(&mutex);
        ret = remove_from_list_by_sockfd(list_of_client, client_sockfd);
    pthread_mutex_unlock(&mutex);
    if(ret == E_CODE)
    {
        printf("Failed remove client from list\n");
        return ret;
    }
    return ret;
}

// Wait message from client and resend to another clients
void *wait_msg(void *arg)
{
    int sockfd = *(int*)arg;
    int ret = E_OK_CODE;
    char buff[MAX_MSG_LEN];
    while(1)
    {
        memset(buff,0, MAX_MSG_LEN);
        ret = read(sockfd, buff, sizeof(buff));
        if(ret == E_CODE)
        {
            printf("Error get message from client (%d)(%s)\n",
                   sockfd, strerror(errno));
            break;
        }
        if(ret != 0)
        {
            printf("Receive message from client(%d): %s\n", sockfd, buff);
            pthread_mutex_lock(&mutex);
                ret = send_msg_to_all_clients(list_of_client, sockfd,
                                              buff, sizeof(buff));
            pthread_mutex_unlock(&mutex);
            if(ret == E_CODE)
            {
                printf("Failed send message to all clients\n");
                break;
            }
        }
        else
        {
            printf("Client with sockfd %d disconnected\n", sockfd);
            ret = remove_client_from_list(sockfd);
            if(ret == E_CODE)
            {
                printf("Failed remove client from list\n");
                break;
            }
            printf("Client(%d) removed from list\n", sockfd);
            break;
        }
    }
    return NULL;
}

int wait_clients(int server_sockfd)
{
    int client_sockfd, ret = E_OK_CODE;
    struct sockaddr_in client_addr;
    unsigned int client_addr_len;
    char ip_str[IPV4_STR_ADDR_LEN];
    pthread_t thread;
    while(1)
    {
        client_addr_len = sizeof(client_addr);
        memset(&client_addr, 0, client_addr_len);
        client_sockfd = accept(server_sockfd,
                               (sockaddrs_s*)&client_addr, &client_addr_len);
        if (client_sockfd == E_CODE)
        {
            printf("Server acccept failed(%s)\n", strerror(errno));
            ret = E_CODE;
            break;
        }
        if(inet_ntop(AF_INET, &client_addr.sin_addr,
                     ip_str, IPV4_STR_ADDR_LEN) == NULL)
        {
            printf("Failed convert IP address to string(%s)\n",
                   strerror(errno));
            ret = E_CODE;
            break;
        }
        printf("Server acccept the client with IP: %s PORT: %d\n",
               ip_str, client_addr.sin_port);

        ret = add_client_to_list(client_sockfd, client_addr);
        if(ret == E_CODE)
        {
            printf("Failed add client\n");
            break;
        }
        printf("Current count of clients: %lu\n", list_of_client->size);

        /*ret = _print_all_clients(list_of_client);
        if(ret == E_CODE)
        {
            printf("Failed print list of clients\n");
            break;
        }*/

        // Create thread for waiting message from client
        ret = pthread_create(&thread, NULL, wait_msg, (void *)&client_sockfd);
        if(ret != E_OK_CODE)
        {
            printf("Failed pthread_create with error(%d): %s\n",
                   ret, strerror(ret));
            break;
        }
        ret = pthread_detach(thread);
        if(ret != E_OK_CODE)
        {
            printf("Failed pthread_detach with error(%d): %s\n",
                   ret, strerror(ret));
            break;
        }
    }
    return ret;
}

int init_server(int max_conn_wait, unsigned long port)
{
    int server_sockfd;
    struct sockaddr_in server_addr;
    int ret = E_OK_CODE;

    server_sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_sockfd == E_CODE)
    {
        printf("Socket creation failed(%s)\n", strerror(errno));
        return E_CODE;
    }
    printf("Socket successfully created\n");
    int true = 1;
    ret = setsockopt(server_sockfd,SOL_SOCKET,SO_REUSEADDR,&true,sizeof(int));
    if(ret == E_CODE)
    {
        printf("Failed set socket options(%s)\n", strerror(errno));
        return ret;
    }

    memset(&server_addr, 0, sizeof (server_addr));

    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(port);

    if ((bind(server_sockfd,
              (sockaddrs_s *)&server_addr, sizeof(server_addr))) == E_CODE)
    {
        printf("Socket bind failed(%s)\n", strerror(errno));
        return E_CODE;
    }
    printf("Socket successfully binded\n");

    if ((listen(server_sockfd, max_conn_wait)) == E_CODE)
    {
        printf("Listen failed(%s)\n", strerror(errno));
        return E_CODE;
    }
    printf("Server listening\n");
    return server_sockfd;
}
