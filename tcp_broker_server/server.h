#ifndef SERVER_H
#define SERVER_H

/****************************************************************************
 * Name:          init_server                                               *
 * Description:   Init server                                               *
 * Parameters:    max_conn(in) :maximum client wich will wait accept        *
 *                port(in) :port wich will listen                           *
 * Return Values: server socker file descriptor or E_CODE                   *
 ***************************************************************************/
int init_server(int max_conn, unsigned long port);

/****************************************************************************
 * Name:          wait_clients                                              *
 * Description:   Wait connect clients and create thread for each clients   *
 *                for processing messages                                   *
 * Parameters:    server_sockfd(in) :server socker file descriptor          *
 * Return Values: E_OK_CODE or E_CODE                                       *
 ***************************************************************************/
int wait_clients(int server_sockfd);

#endif // SERVER_H
